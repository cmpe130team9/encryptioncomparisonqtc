#ifndef GENERIC_H
#define GENERIC_H

//Modes
#define 	DIR_ENCRYPT 	0 		//Are we encrpyting?
#define 	DIR_DECRYPT 	1 		//Are we decrpyting?
#define 	MODE_ECB 		1 		//Are we ciphering in ECB mode?
#define 	MODE_CBC 		2 		//Are we ciphering in CBC mode?
#define 	MODE_CFB1 		3 		//Are we ciphering in 1-bit CFB mode?

//Error Codes
#define 	BAD_KEY_DIR 		-1	//Key direction is invalid (unknown value)
#define 	BAD_KEY_MAT 		-2	//Key material not of correct length
#define 	BAD_KEY_INSTANCE 	-3	//Key passed is not valid
#define 	BAD_CIPHER_MODE 	-4 	//Params struct passed to cipherInit invalid
#define 	BAD_CIPHER_STATE 	-5 	//Cipher in wrong state (e.g., not initialized)

#define 	TRUE 			1
#define 	FALSE 			0

#define		BLOCK_SIZE			128	// number of bits per block

#define		MAX_KEY_SIZE		64	// # of ASCII chars needed to represent a key
#define		MAX_IV_SIZE_S		32	// # of bytes needed to represent an IV (for Serpent)
#define		MAX_IV_SIZE_F		16	// # of bytes needed to represent an IV (for Serpent)

typedef unsigned char BYTE;
typedef	unsigned long DWORD;
typedef DWORD fullSbox[4][256];


#endif // GENERIC_H
