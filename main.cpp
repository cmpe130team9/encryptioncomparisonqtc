//#include "mainwindow.h"
//#include <QApplication>
#include <QCoreApplication>
//#include <QtCharts/QChartView>
//#include <QtCharts/QLineSeries>
#include <QDebug>

#include <iostream>
#include <cmath>
#include <fstream>
#include <chrono>

#include "Rijndael/rijndael.h"
#include "Serpent/serpent.h"
#include "Twofish/twofish.h"

using namespace std;
//QT_CHARTS_USE_NAMESPACE

const int MB = 1048576;

struct inputFile
{
    string name;
    size_t size;
};

//Function to convert unsigned char to string of length 2
void Char2Hex(unsigned char ch, char* szHex)
{
    unsigned char byte[2];
    byte[0] = ch/16;
    byte[1] = ch%16;
    for(int i=0; i<2; i++)
    {
        if(byte[i] >= 0 && byte[i] <= 9)
            szHex[i] = '0' + byte[i];
        else
            szHex[i] = 'A' + byte[i] - 10;
    }
    szHex[2] = 0;
}

//Function to convert string of length 2 to unsigned char
void Hex2Char(char const* szHex, unsigned char& rch)
{
    rch = 0;
    for(int i=0; i<2; i++)
    {
        if(*(szHex + i) >='0' && *(szHex + i) <= '9')
            rch = (rch << 4) + (*(szHex + i) - '0');
        else if(*(szHex + i) >='A' && *(szHex + i) <= 'F')
            rch = (rch << 4) + (*(szHex + i) - 'A' + 10);
        else
            break;
    }
}

//Function to convert string of unsigned chars to string of chars
void CharStr2HexStr(unsigned char const* pucCharStr, char* pszHexStr, int iSize)
{
    int i;
    char szHex[3];
    pszHexStr[0] = 0;
    for(i=0; i<iSize; i++)
    {
        Char2Hex(pucCharStr[i], szHex);
        strcat(pszHexStr, szHex);
    }
}

//Function to convert string of chars to string of unsigned chars
void HexStr2CharStr(char const* pszHexStr, unsigned char* pucCharStr, int iSize)
{
    int i;
    unsigned char ch;
    for(i=0; i<iSize; i++)
    {
        Hex2Char(pszHexStr+2*i, ch);
        pucCharStr[i] = ch;
    }
}

//Run basic tests on Rijndael algoritm, returns 0 iff test passes
int TestRijndael(int mode,int keySize)
{
    const int MAX_BLK_CNT = 4;   // max # blocks per call
    const char chain[] = "\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0";

    BYTE  plainText[MAX_BLK_CNT*(BLOCK_SIZE/8)];
    BYTE cipherText[MAX_BLK_CNT*(BLOCK_SIZE/8)];
    BYTE decryptOut[MAX_BLK_CNT*(BLOCK_SIZE/8)];

    DWORD key32[MAX_KEY_BITS/32];
    unsigned int  i,byteCnt;

    //create key
    for (i=0;i<keySize/32;i++)
        key32[i]=0x10003 * rand();

    //create input data
    byteCnt = (BLOCK_SIZE/8) * (1 + (rand() % MAX_BLK_CNT));
    for (i=0;i<byteCnt;i++)
        plainText[i]=(BYTE) rand();

    try
    {
        Rijndael oRijndael;

        oRijndael.makeKey((char*)key32, chain, keySize/8, 16);

        //encrypt data
        oRijndael.encrypt((char*)plainText, (char*)cipherText, byteCnt, mode);

        //decrypt data
        oRijndael.ResetChain();
        oRijndael.decrypt((char*)cipherText, (char*)decryptOut, byteCnt, mode);


        //make sure the decrypt output matches original plaintext
        if (memcmp(plainText,decryptOut,byteCnt))
            return 1;
    }
    catch(exception& roException)
    {
        qWarning() << "Caught Exception: " << roException.what() << endl;
        return 1;
    }

    return 0;
}

//Run basic tests on Serpent algoritm, returns 0 iff test passes
int TestSerpent(int mode,int keySize)
{
    const int MAX_BLK_CNT = 4;   // max # blocks per call
    BYTE  plainText[MAX_BLK_CNT*(BLOCK_SIZE/8)];
    BYTE cipherText[MAX_BLK_CNT*(BLOCK_SIZE/8)];
    BYTE decryptOut[MAX_BLK_CNT*(BLOCK_SIZE/8)];
    BYTE iv[BLOCK_SIZE/4];
    DWORD key32[MAX_KEY_BITS/16];
    int  i,byteCnt;

    Serpent oSerpent;

    // select key bits
    for (i=0;i<keySize/32;i++)
        key32[i]=0x10003 * rand();

    //run the key schedule
    oSerpent.makeKey(DIR_ENCRYPT,keySize,(char*)key32);

    if (mode != MODE_ECB)		// set up random iv (if needed)
    {
        for (i=0;i<sizeof(iv);i++)
            iv[i]=(BYTE) rand();
    }

    if (oSerpent.cipherInit(mode,(char*)iv) != TRUE)
        return 1;

    /* select number of bytes to encrypt (multiple of block) */
    /* e.g., byteCnt = 16, 32, 48, 64 */
    byteCnt = (BLOCK_SIZE/8) * (1 + (rand() % MAX_BLK_CNT));

    for (i=0;i<byteCnt;i++)		/* generate test data */
        plainText[i]=(BYTE) rand();

    //encrypt the bytes
    if (oSerpent.blockEncrypt(plainText,byteCnt*8,cipherText) != byteCnt*8)
        return 1;

    //re-init the IV (if needed)
    if (mode != MODE_ECB)
        //oSerpent.cipherInit(mode,(char*)iv);
        oSerpent.setIV(iv);
        //memcpy(ci.iv32,iv,sizeof(ci.iv32));

    //decrypt the bytes
    if (oSerpent.blockDecrypt(cipherText,byteCnt*8,decryptOut) != byteCnt*8)
        return 1;

    //make sure the decrypt output matches original plaintext
    if (memcmp(plainText,decryptOut,byteCnt))
        return 1;

    return 0;					// tests passed!

}
int TestSerpent()
{
    Serpent oSerpent;
    keyInstanceS keyI;

    unsigned long x[4];
    char tmpstr[100];
    int rc;

    //Serpent portion

    char IV[]="";
    char key[]="0000000000000000000000000000000000000000000000000000000000000000";
    rc=oSerpent.cipherInit(MODE_ECB, IV);//creates and checks if valid cipher
    if(rc<=0) exit(2);

    /* Let the plaintext be 0x00000000,0x00000001,0x00000002,0x00000003 */

    x[0] = 163473; x[1] = 78689; x[2] = 200083; x[3] = 14763;

    rc=oSerpent.makeKey(DIR_ENCRYPT, 128, key);//create and check if valid key
    if(rc<=0) exit(2);

    qInfo()<<oSerpent.serpent_convert_to_string(128,x,tmpstr)<< endl;//print plaintext
    qInfo()<<oSerpent.serpent_convert_to_string(128,keyI.key,tmpstr)<< endl;//print key
    rc=oSerpent.blockEncrypt((BYTE*)x, 128, (BYTE*)x);//encrypt block
    if(rc<=0) exit(2);
    qInfo()<<oSerpent.serpent_convert_to_string(128,x,tmpstr)<< endl;//print ciphertext
    rc=oSerpent.blockDecrypt((BYTE*)x, 128, (BYTE*)x);//decrypt block
    if(rc<=0) exit(2);
    qInfo()<<oSerpent.serpent_convert_to_string(128,x,tmpstr)<< endl;//print Decrypt back into plaintext

    return 0;
}

//Run basic tests on Twofish algoritm, returns 0 iff test passes
int TestTwofish(int mode,int keySize)
{
    const int MAX_BLK_CNT = 4;   // max # blocks per call
    BYTE  plainText[MAX_BLK_CNT*(BLOCK_SIZE/8)];
    BYTE cipherText[MAX_BLK_CNT*(BLOCK_SIZE/8)];
    BYTE decryptOut[MAX_BLK_CNT*(BLOCK_SIZE/8)];
    BYTE iv[BLOCK_SIZE/8];
    DWORD key32[MAX_KEY_BITS/32];
    int  i,byteCnt;

    Twofish oTwofish;

    //'dummy' setup for a 128-bit key
    if (oTwofish.makeKey(DIR_ENCRYPT,keySize,nullptr) != TRUE)
        return 1;

    //'dummy' setup for cipher
    if (oTwofish.cipherInit(mode,nullptr) != TRUE)
        return 1;

    // select key bits
    for (i=0;i<keySize/32;i++)
        key32[i]=0x10003 * rand();

    //run the key schedule
    oTwofish.makeKey(DIR_ENCRYPT,keySize,(const char*)key32);

    if (mode != MODE_ECB)		/* set up random iv (if needed)*/
    {
        for (i=0;i<sizeof(iv);i++)
            iv[i]=(BYTE) rand();
        //memcpy(ci.iv32,iv,sizeof(ci.iv32));	/* copy the IV to ci */
        oTwofish.setIV(iv);
    }

    /* select number of bytes to encrypt (multiple of block) */
    /* e.g., byteCnt = 16, 32, 48, 64 */
    byteCnt = (BLOCK_SIZE/8) * (1 + (rand() % MAX_BLK_CNT));

    for (i=0;i<byteCnt;i++)		/* generate test data */
        plainText[i]=(BYTE) rand();

    //encrypt the bytes
    if (oTwofish.blockEncrypt(plainText,byteCnt*8,cipherText) != byteCnt*8)
        return 1;

    //re-init the IV (if needed)
    if (mode != MODE_ECB)
        oTwofish.setIV(iv);
        //memcpy(ci.iv32,iv,sizeof(ci.iv32));

    //decrypt the bytes
    if (oTwofish.blockDecrypt(cipherText,byteCnt*8,decryptOut) != byteCnt*8)
        return 1;

    //make sure the decrypt output matches original plaintext
    if (memcmp(plainText,decryptOut,byteCnt))
        return 1;

    return 0;					// tests passed!
}



//Run timed encryption and decryption using Rijndael for all key sizes
int runRijndael(BYTE* key, BYTE* dataIn, BYTE* cipher, BYTE* dataOut, size_t fileSize, size_t results[], int mode)
{
    const char chain[] = "\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0";

    //initialize timer
    using resolution = chrono::nanoseconds;
    chrono::steady_clock::time_point start;
    chrono::steady_clock::time_point finish;

    //for each key size
    for(int i = 0; i<3; i++)
    {
        Rijndael oRijndael;
        oRijndael.makeKey((char*)key, chain, (16 +i*8), 16);    //WARNING: key and block size is in bytes for this one

        start = chrono::high_resolution_clock::now();                               //start timer
        oRijndael.encrypt((char*)dataIn, (char*)cipher, fileSize, mode);            //encrypt input to cipher array using Rijndael
        finish = chrono::high_resolution_clock::now();                              //stop timer
        results[i*2] = chrono::duration_cast<resolution>(finish - start).count();   //save encryption time

        oRijndael.ResetChain();

        start = chrono::high_resolution_clock::now();                               //start timer
        oRijndael.decrypt((char*)cipher, (char*)dataOut, fileSize, mode);           //decrypt cipher to output array using Rijndael
        finish = chrono::high_resolution_clock::now();                              //stop timer
        results[i*2+1] = chrono::duration_cast<resolution>(finish - start).count(); //save encryption time

        //sanity check compare input vs output
        if (memcmp(dataIn,dataOut,fileSize))
            return 1;
    }
    return 0;
}

//Run timed encryption and decryption using Serpent for all key sizes
int runSerpent(BYTE* key, BYTE* dataIn, BYTE* cipher, BYTE* dataOut, size_t fileSize, size_t results[], int mode)
{
    //initialize timer
    using resolution = chrono::nanoseconds;
    chrono::steady_clock::time_point start;
    chrono::steady_clock::time_point finish;

    //for each key size
    for(int i = 0; i<3; i++)
    {
        Serpent oSerpent;

        oSerpent.makeKey(DIR_ENCRYPT,(128 +i*16),(char*)key);
        oSerpent.cipherInit(mode,nullptr);  //TODO: set an IV if we want any mode other than ECB

        start = chrono::high_resolution_clock::now();                               //start timer
        oSerpent.blockEncrypt(dataIn,fileSize*8,cipher);                            //encrypt input to cipher array using Rijndael
        finish = chrono::high_resolution_clock::now();                              //stop timer
        results[i*2] = chrono::duration_cast<resolution>(finish - start).count();   //save encryption time

        //TODO: reset IV if using any mode other than ECB

        start = chrono::high_resolution_clock::now();                               //start timer
        oSerpent.blockDecrypt(cipher,fileSize*8,dataOut);                           //decrypt cipher to output array using Rijndael
        finish = chrono::high_resolution_clock::now();                              //stop timer
        results[i*2+1] = chrono::duration_cast<resolution>(finish - start).count(); //save encryption time

        //sanity check compare input vs output
        if (memcmp(dataIn,dataOut,fileSize))
            return 1;
    }

    return 0;
}

//Run timed encryption and decryption using Twofish for all key sizes
int runTwofish(BYTE* key, BYTE* dataIn, BYTE* cipher, BYTE* dataOut, size_t fileSize, size_t results[], int mode)
{
    //initialize timer
    using resolution = chrono::nanoseconds;
    chrono::steady_clock::time_point start;
    chrono::steady_clock::time_point finish;

    //for each key size
    for(int i = 0; i<3; i++)
    {
        Twofish oTwofish;

        oTwofish.makeKey(DIR_ENCRYPT,(128 +i*16),(const char*)key);
        oTwofish.cipherInit(mode,nullptr);  //TODO: set an IV if we want any mode other than ECB

        start = chrono::high_resolution_clock::now();                               //start timer
        oTwofish.blockEncrypt(dataIn,fileSize*8,cipher);                            //encrypt input to cipher array using Rijndael
        finish = chrono::high_resolution_clock::now();                              //stop timer
        results[i*2] = chrono::duration_cast<resolution>(finish - start).count();   //save encryption time

        //TODO: reset IV if using any mode other than ECB

        start = chrono::high_resolution_clock::now();                               //start timer
        oTwofish.blockDecrypt(cipher,fileSize*8,dataOut);                           //decrypt cipher to output array using Rijndael
        finish = chrono::high_resolution_clock::now();                              //stop timer
        results[i*2+1] = chrono::duration_cast<resolution>(finish - start).count(); //save encryption time

        //sanity check compare input vs output
        if (memcmp(dataIn,dataOut,fileSize))
            return 1;
    }

    return 0;
}


int main(int argc, char *argv[])
{
    QCoreApplication app(argc, argv);

    srand((unsigned) time(nullptr));	//seed random number gen


    //------------------------------------------
    // basic tests on each algorithm
    //------------------------------------------
    int testCnt,keySize;

    //Rijndael Tests
    for (keySize=128;keySize<=256;keySize+=64)
        for (testCnt=0;testCnt<10;testCnt++)
        {
            if (TestRijndael(MODE_ECB,keySize))
            {
                qWarning() <<"Rijndael ECB Failure at keySize=" << keySize << " test" << testCnt << endl;
            }

            if (TestRijndael(MODE_CBC,keySize))
            {
                qWarning() <<"Rijndael CBC Failure at keySize=" << keySize << " test" << testCnt << endl;
            }
        }

    //Serpent Tests
    for (keySize=128;keySize<=256;keySize+=64)
        for (testCnt=0;testCnt<10;testCnt++)
        {
            if (TestSerpent(MODE_ECB,keySize))
            {
                qWarning() <<"Serpent ECB Failure at keySize=" << keySize << " test" << testCnt << endl;
            }
            //TODO: figure out why Serpent CBC mode doesn't work
            /*if (TestSerpent(MODE_CBC,keySize))
            {
                qWarning() <<"Serpent CBC Failure at keySize=" << keySize << " test" << testCnt << endl;
            }*/
        }

    //Twofish Tests
    for (keySize=128;keySize<=256;keySize+=64)
        for (testCnt=0;testCnt<10;testCnt++)
        {
            if (TestTwofish(MODE_ECB,keySize))
            {
                qWarning() <<"Twofish ECB Failure at keySize=" << keySize << " test" << testCnt << endl;
            }
            if (TestTwofish(MODE_CBC,keySize))
            {
                qWarning() <<"Twofish CBC Failure at keySize=" << keySize << " test" << testCnt << endl;
            }
        }


    //------------------------------------------
    // Runtime Comparison
    //------------------------------------------

    const int fileCount = 38;
    int fileNum = 0;

    //array of file names and sizes
    inputFile inputFiles[fileCount] =
    {
        {"Binary1.bin",0},
        {"Binary1.bin",8*MB},
        {"Binary1.bin",6*MB},
        {"Binary1.bin",4*MB},
        {"Binary1.bin",2*MB},
        {"Binary2.bin",0},
        {"Binary2.bin",8*MB},
        {"Binary2.bin",6*MB},
        {"Binary2.bin",4*MB},
        {"Binary2.bin",2*MB},
        {"Image1a.png",0},
        {"Image1b.png",0},
        {"Image1c.png",0},
        {"Image1d.png",0},
        {"Image1e.png",0},
        {"Image2a.png",0},
        {"Image2b.png",0},
        {"Image2c.png",0},
        {"Image2d.png",0},
        {"Image2e.png",0},
        {"Text1.txt",0},
        {"Text1.txt",3*MB},
        {"Text1.txt",2*MB},
        {"Text1.txt",1*MB},
        {"Text2.txt",0},
        {"Text2.txt",3*MB},
        {"Text2.txt",2*MB},
        {"Text2.txt",1*MB},
        {"Audio1.m4a",10*MB},
        {"Audio1.m4a",8*MB},
        {"Audio1.m4a",6*MB},
        {"Audio1.m4a",4*MB},
        {"Audio1.m4a",2*MB},
        {"Audio2.aif",10*MB},
        {"Audio2.aif",8*MB},
        {"Audio2.aif",6*MB},
        {"Audio2.aif",4*MB},
        {"Audio2.aif",2*MB}
    };

    BYTE* dataIn  = new BYTE[12582912];     //input data array
    BYTE* cipher  = new BYTE[12582912];     //cipher data array
    BYTE* dataOut = new BYTE[12582912];     //output data array

    BYTE* key = (BYTE*)"This is the key for encryption!!";

    //result storage (enc/dec, key size, file)
    size_t sizes[fileCount] ={};
    size_t resultsRijndael[fileCount*6] ={};
    size_t resultsSerpent[fileCount*6] ={};
    size_t resultsTwofish[fileCount*6] ={};

    ifstream inStream;  //input file stream
    size_t fileSize=0;  //file size

    //for each file
    for (inputFile file : inputFiles)
    {
        inStream.open("Input Files/"+file.name);   //open file stream

        //skip this file if it can't be opened
        if (!inStream.good() || inStream.peek() == std::ifstream::traits_type::eof())
        {
            sizes[fileNum] = 0;
            qWarning() << "Unable to open file " << file.name.c_str() << endl;
            continue;
        }

        //get file size
        inStream.seekg (0, inStream.end);   //seek to end
        fileSize = inStream.tellg();        //store file size
        inStream.seekg (0, inStream.beg);   //seek to beginning


        //if file size limit, apply it
        if (file.size)
            fileSize = min(file.size,fileSize);

        //read file into input array and close file
        inStream.read((char*)dataIn, fileSize);
        inStream.close();

        //pad input array if needed
        if (fileSize%(BLOCK_SIZE/8))
        {
            unsigned int padding = (BLOCK_SIZE/8) - (fileSize%(BLOCK_SIZE/8));
            memset(dataIn+fileSize,0,padding);
            fileSize += padding;
        }

        //store file size
        sizes[fileNum] = fileSize;

        //Run Rijndael time tests
        qInfo() << "Running Rijndael on" << file.name.c_str() <<fileSize<< endl;
        if(runRijndael(key, dataIn, cipher, dataOut, fileSize, &resultsRijndael[fileNum*6], MODE_ECB))
            qWarning() <<"Rijndael error on file" << file.name.c_str() << endl;

        //Run Serpent time tests
        qInfo() << "Running Serpent on" << file.name.c_str() <<fileSize<< endl;
        if(runSerpent(key, dataIn, cipher, dataOut, fileSize, &resultsSerpent[fileNum*6], MODE_ECB))
            qWarning() <<"Serpent error on file" << file.name.c_str() << endl;

        //Run Twofish time tests
        qInfo() << "Running Twofish on" << file.name.c_str() <<fileSize<< endl;
        if(runTwofish(key, dataIn, cipher, dataOut, fileSize, &resultsTwofish[fileNum*6], MODE_ECB))
            qWarning() <<"Twofish error on file" << file.name.c_str() << endl;

        fileNum++;
    }

    //open output file
    ofstream outStream("Results.csv");

    //write result data to file in csv format
    outStream << "File,Size,";
    outStream << "Encrypt R128,Decrypt R128,Encrypt R192,Decrypt R192,Encrypt R256,Decrypt R256,";
    outStream << "Encrypt S128,Decrypt S128,Encrypt S192,Decrypt S192,Encrypt S256,Decrypt S256,";
    outStream << "Encrypt T128,Decrypt T128,Encrypt T192,Decrypt T192,Encrypt T256,Decrypt T256" << endl;

    for (int i=0; i<fileCount;i++)
    {
        outStream << inputFiles[i].name << ","<< sizes[i];
        for (int j=0; j<6; j++) outStream <<","<<resultsRijndael[i*6+j];
        for (int j=0; j<6; j++) outStream <<","<<resultsSerpent[i*6+j];
        for (int j=0; j<6; j++) outStream <<","<<resultsTwofish[i*6+j];
        outStream << endl;
    }

    //flush and close file
    outStream.flush();
    outStream.close();

    //diplay results using Qt charts?
}
