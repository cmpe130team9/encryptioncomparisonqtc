#ifndef RIJNDAEL_H
#define RIJNDAEL_H

#include "generic.h"

class Rijndael
{
public:
    Rijndael();
    virtual ~Rijndael();

    //Expand user-supplied key material into a session key.
    // key        - The 128/192/256-bit user-key to use.
    // chain      - initial chain block for CBC and CFB modes.
    // keylength  - 16, 24 or 32 bytes
    // blockSize  - The block size in bytes of this Rijndael (16, 24 or 32 bytes).
    void makeKey(char const* key, char const* chain, int keyLength=DEFAULT_BLOCK_SIZE, int blockSize=DEFAULT_BLOCK_SIZE);

    //encrypt or decrypt some data
    void encrypt(char const* in, char* result, size_t n, int iMode=MODE_ECB);
    void decrypt(char const* in, char* result, size_t n, int iMode=MODE_ECB);

    //encrypt or decrypt exactly one block of data
    void encryptBlock(char const* in, char* result);
    void decryptBlock(char const* in, char* result);

    int GetKeyLength();
    int	GetBlockSize();
    int GetRounds();

    void ResetChain();


    static const char *c_chain0;   //Null chain

private:
    enum { DEFAULT_BLOCK_SIZE=16 }; //in bytes
    enum { MAX_BLOCK_SIZE=32, MAX_ROUNDS=14, MAX_KC=8, MAX_BC=8 };

    //(en/de)crypt exactly one block of plaintext, assuming Rijndael's default block size (128-bit).
    void defEncryptBlock(char const* in, char* result);
    void defDecryptBlock(char const* in, char* result);

    //Auxiliary Functions
    static int Mul(int a, int b);       //Multiply two elements of GF(2^m)
    static int Mul4(int a, char b[]);   //Convenience method used in generating Transposition Boxes
    void Xor(char* buff, char const* chain);

    static const int c_alog[256];
    static const int c_log[256];
    static const char c_S[256];
    static const char c_Si[256];
    static const int c_T1[256];
    static const int c_T2[256];
    static const int c_T3[256];
    static const int c_T4[256];
    static const int c_T5[256];
    static const int c_T6[256];
    static const int c_T7[256];
    static const int c_T8[256];
    static const int c_U1[256];
    static const int c_U2[256];
    static const int c_U3[256];
    static const int c_U4[256];
    static const char c_rcon[30];
    static const int c_shifts[3][4][2];


    //Error Messages
    static const char *c_szErrorMsg1;
    static const char *c_szErrorMsg2;

    bool bKeyInit;                //Key Initialization Flag
    int Ke[MAX_ROUNDS+1][MAX_BC]; //Encryption (m_Ke) round key
    int Kd[MAX_ROUNDS+1][MAX_BC]; //Decryption (m_Kd) round key
    int keyLength;                //Key Length
    int	blockSize;                //Block Size
    int iRounds;                  //Number of Rounds
    char chain0[MAX_BLOCK_SIZE];  //Chain Block
    char chain[MAX_BLOCK_SIZE];
    int tk[MAX_KC];                 //Auxiliary private use buffers
    int a[MAX_BC];
    int t[MAX_BC];
};

#endif // RIJNDAEL_H
