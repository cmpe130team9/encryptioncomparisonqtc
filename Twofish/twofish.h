#ifndef TWOFISH_H
#define TWOFISH_H

#include "generic.h"

//--------------------------------------------------------
//TODO: Get Rid of this junk
#define		BAD_INPUT_LEN		-6	/* inputLen not a multiple of block size */
#define		BAD_PARAMS			-7	/* invalid parameters */
#define		BAD_IV_MAT			-8	/* invalid IV text */
#define		BAD_ENDIAN			-9	/* incorrect endianness define */
#define		BAD_ALIGN32			-10	/* incorrect 32-bit alignment */


#define		MAX_ROUNDS			 16	/* max # rounds (for allocating subkey array) */
#define		ROUNDS_128			 16	/* default number of rounds for 128-bit keys*/
#define		ROUNDS_192			 16	/* default number of rounds for 192-bit keys*/
#define		ROUNDS_256			 16	/* default number of rounds for 256-bit keys*/
#define		MAX_KEY_BITS		256	/* max number of bits of key */
#define		MIN_KEY_BITS		128	/* min number of bits of key (zero pad) */
#define		VALID_SIG	 0x48534946	/* initialization signature ('FISH') */
#define		MCT_OUTER			400	/* MCT outer loop */
#define		MCT_INNER		  10000	/* MCT inner loop */
#define		REENTRANT			  1	/* nonzero forces reentrant code (slightly slower) */

#define		INPUT_WHITEN		0	/* subkey array indices */
#define		OUTPUT_WHITEN		( INPUT_WHITEN + BLOCK_SIZE/32)
#define		ROUND_SUBKEYS		(OUTPUT_WHITEN + BLOCK_SIZE/32)	/* use 2 * (# rounds) */
#define		TOTAL_SUBKEYS		(ROUND_SUBKEYS + 2*MAX_ROUNDS)

/* API to check table usage, for use in ECB_TBL KAT */
#define		TAB_DISABLE			0
#define		TAB_ENABLE			1
#define		TAB_RESET			2
#define		TAB_QUERY			3
#define		TAB_MIN_QUERY		50

#if BLOCK_SIZE == 128			/* optimize block copies */
    #define		Copy1(d,s,N)	((DWORD *)(d))[N] = ((DWORD *)(s))[N]
    #define		BlockCopy(d,s)	{ Copy1(d,s,0);Copy1(d,s,1);Copy1(d,s,2);Copy1(d,s,3); }
#else
    #define		BlockCopy(d,s)	{ memcpy(d,s,BLOCK_SIZE/8); }
#endif

//--------------------------------------------------------

class Twofish
{
public:
    Twofish();
    virtual ~Twofish();

    /*
    void makeKey(char const* key, char const* chain, int keylength, int blockSize);

    //encrypt or decrypt some data
    void encrypt(char const* in, char* result, size_t n, int iMode=MODE_ECB);
    void decrypt(char const* in, char* result, size_t n, int iMode=MODE_ECB);

    //encrypt or decrypt exactly one block of data
    void encryptBlock(char const* in, char* result);
    void decryptBlock(char const* in, char* result);
    */


    int makeKey(BYTE direction, int keyLen, const char *keyMaterial);
    int cipherInit(BYTE mode, const char *IV);

    int blockEncrypt(const BYTE *input, int inputLen, BYTE *outBuffer);
    int blockDecrypt(const BYTE *input, int inputLen, BYTE *outBuffer);

    int	reKey();	// do key schedule using modified key.keyDwords

    void setIV(BYTE *iv);

private:
    //Consts
    static const int c_numRounds[4];

    //TODO: check where these vars are used, they may be unecessarily copied

    //cypher vars
    BYTE  mode;						// MODE_ECB, MODE_CBC, or MODE_CFB1
    BYTE  IV[MAX_IV_SIZE_F];		// CFB1 iv bytes  (CBC uses iv32)
    DWORD cipherSig;				// set to VALID_SIG by cipherInit()
    DWORD iv32[BLOCK_SIZE/32];		// CBC IV bytes arranged as dwords

    //key vars
    BYTE direction;					// Key used for encrypting or decrypting?
    int  keyLen;					// Length of the key
    char keyMaterial[MAX_KEY_SIZE+4];// Raw key data in ASCII

    // Twofish-specific parameters
    DWORD keySig;					// set to VALID_SIG by makeKey()
    int	  numRounds;				// number of rounds in cipher
    DWORD key32[MAX_KEY_BITS/32];	// actual key bits, in dwords
    DWORD sboxKeys[MAX_KEY_BITS/64];// key bits used for S-boxes
    DWORD subKeys[TOTAL_SUBKEYS];	// round subkeys, input/output whitening bits

#if REENTRANT
    fullSbox sBox8x32;				/* fully expanded S-box */
#endif


    //private utility functions
    int ParseHexDword(int bits,const char *srcTxt,DWORD *d,char *dstTxt);
    int TableOp(int op);
    DWORD f32(DWORD x,const DWORD *k32,int keyLen);
    void Xor256(void *dst,const void *src,BYTE b);
    void BuildMDS(void);
    DWORD RS_MDS_Encode(DWORD k0,DWORD k1);
    void ReverseRoundSubkeys(BYTE newDir);
};

#endif // TWOFISH_H
