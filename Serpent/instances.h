#ifndef INSTANCES_H
#define INSTANCES_H

#include "generic.h"

/*  The structure for key information */
typedef struct {
      BYTE  direction;	/*  Key used for encrypting or decrypting? */
      int   keyLen;	/*  Length of the key  */
      char  keyMaterial[MAX_KEY_SIZE+1];  /*  Raw key data in ASCII, e.g.,
                        what the user types or KAT values)*/
      /*  The following parameters are algorithm dependent, replace or
            add as necessary  */
      unsigned long key[8];             /* The key in binary */
      unsigned long subkeys[33][4];	/* Serpent subkeys */
      } keyInstanceS;

/*  The structure for cipher information */
typedef struct {
      BYTE	mode;           // MODE_ECB, MODE_CBC, or MODE_CFB1
      char  IV[MAX_IV_SIZE_S]; 	// A possible Initialization Vector for ciphering
      /*  Add any algorithm specific parameters needed here  */
      int   blockSize;    	// Sample: Handles non-128 bit block sizes (if available)
      } cipherInstanceS;


#endif // INSTANCES_H
