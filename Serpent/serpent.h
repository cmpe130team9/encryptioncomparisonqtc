#ifndef SERPENT_H
#define SERPENT_H

#include "generic.h"
#include "instances.h"
class Serpent
{
public:
    Serpent();
    virtual ~Serpent();

    /*  Function protoypes  */
    int makeKey(BYTE direction, int keyLen, char *keyMaterial);

    int cipherInit(BYTE mode, char *IV);

    int blockEncrypt(BYTE *input,int inputLen, BYTE *outBuffer);
    int blockDecrypt(BYTE *input,int inputLen, BYTE *outBuffer);


    void serpent_encrypt(unsigned long plaintext[4],
            unsigned long ciphertext[4],
            unsigned long subkeys[33][4]);
    void serpent_decrypt(unsigned long ciphertext[4],
            unsigned long plaintext[4],
            unsigned long subkeys[33][4]);

    void setIV(BYTE *iv);

    int serpent_convert_from_string(int len, char *str, unsigned long *val);
    char* serpent_convert_to_string(int len, unsigned long val[8], char *str);

private:
    cipherInstanceS *cipher;
    keyInstanceS *key;
};



#endif // SERPENT_H
